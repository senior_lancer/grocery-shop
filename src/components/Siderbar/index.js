import {List, ListItem, ListItemText, Grid, Typography, Divider, MenuItem, Link } from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const useStyles = makeStyles(theme => ({
    top_divider: {
        background:'#FF9900'
    },
    list_item: {
        '&:hover': {
            background: '#84C639',
            color: '#FFFFFF'
        }
    },
    list_item_txt:{
        paddingLeft:'30px',
        [theme.breakpoints.down('sm')]:{
            paddingLeft: '0px',
            textAlign: "center"
        }
    },
    link: {
        color: '#000000'
    }
}));

const Siderbar = props => {
    const classes = useStyles();

    const handleItemClick = (event) => {
        props.setbrand(event.nativeEvent.target.outerText);
    }

    return(
        <Grid>
            <List component="nav">
                <Divider className={classes.top_divider} />
                <Link className={classes.link} href={'/branded'} underline="none">
                    <MenuItem button className={classes.list_item} onClick={handleItemClick}>
                        <ListItemText className={classes.list_item_txt} primary="Branded Foods" />
                    </MenuItem >
                </Link>
                <Divider />
                <Link className={classes.link} href={'/households'} underline="none">
                    <MenuItem button className={classes.list_item} onClick={handleItemClick}>
                        <ListItemText className={classes.list_item_txt} primary="Households" />
                    </MenuItem>
                </Link>
                <Divider />
                <MenuItem button className={classes.list_item} onClick={handleItemClick}>
                    <ListItemText className={classes.list_item_txt} primary="Veggies & Fruits" />
                    <ArrowDropDownIcon />
                </MenuItem>
                <Divider />
                <MenuItem button className={classes.list_item} onClick={handleItemClick}>
                    <ListItemText className={classes.list_item_txt} primary="Kitchen" />
                </MenuItem>
                <Divider />
                <MenuItem button className={classes.list_item} onClick={handleItemClick}>
                    <ListItemText className={classes.list_item_txt} primary="Short Codes" />
                </MenuItem>
                <Divider />
                <MenuItem button className={classes.list_item} onClick={handleItemClick}>
                    <ListItemText className={classes.list_item_txt} primary="Beverages" />
                    <ArrowDropDownIcon />
                </MenuItem>
                <Divider />
                <MenuItem button className={classes.list_item} onClick={handleItemClick}>
                    <ListItemText className={classes.list_item_txt} primary="Pet Food" />
                </MenuItem>
                <Divider />
                <MenuItem button className={classes.list_item} onClick={handleItemClick}>
                    <ListItemText className={classes.list_item_txt} primary="Frozen Foods" />
                    <ArrowDropDownIcon />
                </MenuItem>
                <Divider />
                <MenuItem button className={classes.list_item} onClick={handleItemClick}>
                    <ListItemText className={classes.list_item_txt} primary="Bread & Bakery" />
                </MenuItem>
            <Divider />
            </List>
        </Grid>
    );
}

export default Siderbar;