import {Button, Grid, Typography, List, CardMedia} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import Lists from './Lists';
import TwitterItem from './TwitterItem';

import CardImage from '../../static/images/card.png';

import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import LanguageIcon from '@material-ui/icons/Language';
import LinkedInIcon from '@material-ui/icons/LinkedIn';

const useStyles = makeStyles(theme=> ({
    content: {
        padding: '4em 0',
        background: '#212121'
    },
    context: {
        width: '1170px',
        paddingRight: "15px",
        paddingLeft: "15px",
        marginRight: "auto",
        marginLeft: "auto",
    },
    twitter_grid: {
        padding:"20px 15px"
    },
    header_txt: {
        fontSize: '1.2em',
        color: '#fff',
        textTransform: 'uppercase',
        marginBottom: '1em'
    },
    pay_txt: {
        color: "#84C639",
        textTransform: "capitalize",
        fontSize: "20px",
        marginBottom: "18px"
    },
    pay_img: {
        height: '31px',
        marginRight: "80px"
    },
    connect_txt: {
        color: "#FFFFFF",
        textTransform: "capitalize",
        fontSize: "20px",
        marginBottom: "12px"
    },
    margin_bottom_28: {
        marginBottom: '28px'
    },
    copy_grid: {
        margin: "4em 0 0",
        padding: '3em 0 0',
        borderTop: '1px solid #3A3A3A'
    },
    copy_txt: {
        fontSize: '14px',
        color: '#999',
        lineHeight: '1.8em'
    },
    facebook: {
        fontSize: '44px',
        color: '#FFFFFF',
        '&:hover': {
            color: "#3B5998"
        }
    },
    twitter: {
        fontSize: '44px',
        color: '#FFFFFF',
        '&:hover': {
            color: "#55ACEE"
        }
    },
    linkedin: {
        fontSize: '44px',
        color: '#FFFFFF',
        '&:hover': {
            color: "#23527c"
        }
    },
    instagram: {
        fontSize: '44px',
        color: '#FFFFFF',
        '&:hover': {
            color: "#833AB4"
        }
    },
    language: {
        fontSize: '44px',
        color: '#FFFFFF',
        '&:hover': {
            color: '#EA4C89',
        }
    }
}));

const event_list = [
    {
        header: "information",
        items: [
            "Event", "About Us", "Best Deals", "Services", "Short Codes"
        ]
    },
    {
        header: "POLICY INFO",
        items: [
            "FAQ", "Privacy Policy", "Terms Of Use"
        ]
    },
    {
        header: "WHAT IN STORES",
        items: [
            "Pet Food", "Frozen Snacks", "Kitchen", "Branded Foods", "Households"
        ]
    }
];

const twitter_list = [
    {
        header: "information",
        description: "Non numquam http://sd.ds/13jklf# eius modi tempora incidunt ut labore et http://sd.ds/1389kjklf#quo nulla." 
    },
    {
        header: "02 day ago",
        description: "Con numquam http://fd.uf/56hfg# eius modi tempora incidunt ut labore et http://fd.uf/56hfg#quo nulla." 
    }
];

const Footer = props => {
    const classes = useStyles();

    return(
        <Grid container className={classes.content} justify="center" direction="row">
            <Grid item container className={classes.context} justify="flex-start" direction="column">
                <Grid item container justify="center" direction="row">
                    { event_list.map(item => <Lists key={item.items} head={item.header} list={item.items}/>) }
                    <Grid item className={classes.twitter_grid} xs={12} sm={6} md={3}>
                        <Grid item container direction='column'>
                            <Typography className={classes.header_txt}>TWITTER POSTS</Typography>
                            <List>
                                { twitter_list.map(item => <TwitterItem key={item.header + item.description} head={item.header} description={item.description} />) }
                            </List>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item container justify="flex-start" direction="row">
                    <Grid item container className={classes.margin_bottom_28} justify="flex-start" direction="column" xs={12} sm={6} md={3}>
                        <Typography className={classes.pay_txt}>100% Secure Payments</Typography>
                        <CardMedia className={classes.pay_img} image={CardImage} />
                    </Grid>
                    <Grid item container className={classes.margin_bottom_28} justify="flex-start" direction="column" xs={12} sm={6} md={3}>
                        <Typography className={classes.connect_txt}>Connect With Us</Typography>
                        <Grid item container justify="flex-start" direction="row">
                            <Grid item xs={2}><FacebookIcon className={classes.facebook}/></Grid>
                            <Grid item xs={2}><TwitterIcon className={classes.twitter}/></Grid>
                            <Grid item xs={2}><LinkedInIcon className={classes.linkedin}/></Grid>
                            <Grid item xs={2}><InstagramIcon className={classes.instagram}/></Grid>
                            <Grid item xs={2}><LanguageIcon className={classes.language}/></Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item container className={classes.copy_grid} justify="center">
                    <Typography className={classes.copy_txt} noWrap>©{(new Date().getFullYear())} Grocery Store. All rights reserved. |</Typography>
                    <Typography className={classes.copy_txt} style={{marginLeft: 8 }}>Developed by Senior Lancer</Typography>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default Footer;

