import {Grid, Typography, List, MenuItem, ListItemText, ListItem, ListItemIcon} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

const useStyles = makeStyles(theme => ({
    content: {
        padding:"20px 15px"
    },
    header_txt: {
        fontSize: '1.2em',
        color: '#fff',
        textTransform: 'uppercase',
        marginBottom: '1em'
    },
    list_item: {
        paddingLeft: '0px'
    },
    list_item_icon: {
        minWidth: '30px'
    },
    list_item_txt: {
        color: '#999',
        '&:hover': {
            color: '#FFFFFF'
        }
    },
    icon: {
        color: "#FFFFFF"
    }
}));

const Lists = props => {
    const classes = useStyles();
    
    const handleItemClick = (event) => {
        alert(event.nativeEvent.target.outerText)
    }

    return(
        <Grid item className={classes.content} xs={12} sm={6} md={3}>
            <Grid item container direction='column'>
                <Typography className={classes.header_txt}>{props.head}</Typography>
                <List key={props.head}>
                    {
                        props.list.map(item => 
                            <ListItem key={item} className={classes.list_item} onClick={handleItemClick}>
                                <ListItemIcon className={classes.list_item_icon}>
                                    <KeyboardArrowRightIcon className={classes.icon}/>
                                </ListItemIcon>
                                <ListItemText className={classes.list_item_txt} primary={item} />
                            </ListItem >   
                        )
                    }
                </List>
            </Grid>
        </Grid>
    );
}

export default Lists;