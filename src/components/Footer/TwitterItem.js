import {ListItem, ListItemIcon, Typography, ListItemText, Grid} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import TwitterIcon from '@material-ui/icons/Twitter';

const useStyles = makeStyles(theme => ({
    list_item: {
        paddingLeft: '0px',
        alignItems: 'flex-start',
    },
    list_item_icon: {
        minWidth: '30px',
        margin: '6px 0'
    },
    list_item_txt: {
        color: '#84C639',
        '&:hover': {
            color: '#84C639'
        }
    },
    icon: {
        color: "#FA1818"
    },
    description_txt: {
        color: "#999",
        '&:hover': {
            color: "#999"
        }
    }
}));

const TwitterItem = props => {
    const classes = useStyles();

    return(
        <ListItem className={classes.list_item}>
            <ListItemIcon className={classes.list_item_icon}>
                <TwitterIcon className={classes.icon} fontSize='small' />
            </ListItemIcon>
            <ListItemText 
                key={props.description}
                className={classes.list_item_txt} 
                primary={props.head}
                secondary={
                    <Typography className={classes.description_txt}>{props.description}</Typography>
                }
            />
        </ListItem >        
    );
}

export default TwitterItem;