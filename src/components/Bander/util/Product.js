import {Button, Grid, Typography, Box, useMediaQuery, Collapse, colors,CardMedia } from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState, useEffect } from 'react';

import ProductImage from '../../../static/images/17.png';
import OfferImage from '../../../static/images/offer.png';

const useStyles = makeStyles(theme => ({
    content: {
        padding: '15px'
    },
    outline_box: {
        position: 'relative',
        background: '#FFFFFF',
        width: '100%',
        border: '1px solid #BEBEBE'
    },
    inline_box: {
        position: 'relative',
        background: '#FFFFFF',
        width: '100%',
        border: '1px solid #D7D7D7',
        margin: '8px',
        padding: '16px'
    },
    title_grid: {
        display: 'table-cell',
    },
    product_title: {
        display: 'table-cell',
        verticalAlign: 'bottom',
        color: '#212121',
        fontSize: '18px',
        height: '54px'
    },
    price_txt: {
        fontSize: '21px',
        color: '#212121',
        fontWeight: 'bold'
    },
    before_price_txt: {
        fontSize: '21px',
        color: '#212121',
        textDecoration: 'line-through',
        paddingLeft: '14px'
    },
    btn_grid: {
        margin: '24px auto'
    },
    cart_btn: {
        background: '#FA1818',
        color: '#FFFFFF',
        fontSize: '14px',
        padding: '8px 24px',
        borderRadius: '0px',
    },
    cart_btn_over: {
        background: '#84C639',
        color: '#FFFFFF',
        fontSize: '14px',
        padding: '8px 24px',
        borderRadius: '0px',
        '&:hover': {
            background: '#84C639'
        }
    },
    product_img: {
        height: '140px',
        width: '140px'
    },
    offer_img: {
        width: '40px',
        height: '40px'
    },
    offer_grid: {
        position: 'absolute'
    }
}));

const Product = props => {
    const classes = useStyles();
    // useEffect = (() => {
        
    // });
    const [getPrice, setPrice] = useState('8.00');
    const [getProductImage, setProductImage] = useState('../../../static/images/17.png');
    const [getHover, setHover] = useState(false);

    const handleProductHover = () => {
        setHover(true);
    }
    
    const handleProductLeave = () => {
        setHover(false);
    }

    return(
        <Grid item className={classes.content} xs={12} sm={6} md={3}>
            <Grid item container className={classes.outline_box} onMouseOver={handleProductHover} onMouseLeave={handleProductLeave}>
                <Grid item container className={classes.inline_box}>
                    <Grid item container direction='column'>
                        <Grid item container justify='center' >
                            <CardMedia image={ProductImage} className={classes.product_img}/>
                        </Grid>
                        <Grid item container className={classes.title_grid} justify='flex-start'>
                            <Typography className={classes.product_title}>{props.name}</Typography>
                        </Grid>
                        <Grid item container justify='flex-start' direction='row'>
                            <Typography className={classes.price_txt}>${ + props.current_price}</Typography>
                            <Typography className={classes.before_price_txt}>${ + props.before_price}</Typography>
                        </Grid>
                        <Grid item container className={classes.btn_grid} justify='center'>
                            <Button className={getHover ? classes.cart_btn_over : classes.cart_btn}>ADD TO CART</Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item className={classes.offer_grid}>
                    <CardMedia className={classes.offer_img} image={OfferImage} />
                </Grid>
            </Grid>
        </Grid>
    );
}

export default Product;

