import {Button, Grid, Typography} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';
import { connect } from "react-redux";
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import HomeProduct from './HomeProduct';
import Sign from './Sign';
import HouseHolds from './Households';
import Branded from './Branded'

import {
    BANNER_ACTION_HOME,
    BANNER_ACTION_SIGN,
} from '../../util/action_type';


const mapStateToProps = state => {
    return { event: state.banner_event };
};

const useStyles = makeStyles(theme => ({

}));

const Product = props => {
    const classes = useStyles();
    return(
        <Grid container>
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={HomeProduct} />
                    <Route path='/sign' component={Sign} />
                    <Route path='/branded' component={Branded} />
                    <Route path='/households' component={HouseHolds} />
                </Switch>
            </BrowserRouter>
        </Grid>
    );
}

export default connect(mapStateToProps)(Product);