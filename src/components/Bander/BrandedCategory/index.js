import {Button, Grid, Typography, Box, useMediaQuery, Zoom , CardMedia, TextField } from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import CategoryImage from '../../../static/images/branded_cateory_1.jpg'; 

const useStyles = makeStyles(theme => ({
    content: {
        paddingRight: '15px',
        [theme.breakpoints.down('md')]: {
            padding: '0 15px'
        }
    },
    category_media: {
        width: '400px',
        height: '266px'
    },
    cagtegory_grid: {
        width: '100%',
        height: '266px',
        background: '#FA1818',
        position: 'absolut'
    },
    category_header: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontSize: '17px',
        padding: '10px 0',
        background: 'rgba(0, 0, 0, 0.8)'
    },
    category_discription: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontSize: '17px',
        padding: '10px 0',
        fontStyle: 'italic'
    },
    header_grid: {
        padding: '18px 0'
    },
    header_txt: {
        fontSize: '32px'
    },
    discription_grid: {
        marginBottom: '44px'
    },
    discription_txt: {
        color: '#999',
        fontSize: '18px'
    },
}));

const BrandedCategory = props => {
    const classes = useStyles();

    const [hover, setHover] = useState(false);

    const handleGridHover = () => {
        setHover(true);
    }

    const handleGridLeave = () => {
        setHover(false);
    }

    return(
        <Grid item container className={classes.content} xs={12} sm={12} md={4} direction='column' onMouseOver={handleGridHover} onMouseLeave={handleGridLeave}>
            <Grid item container>
                {
                    !hover ?
                    <Zoom in={!hover}>
                        <Grid item container justify='center'>
                            <CardMedia className={classes.category_media} image={CategoryImage} />
                        </Grid>
                    </Zoom> :
                    <Zoom in={hover}>
                        <Grid item container className={classes.cagtegory_grid} justify='center' direction='column' >
                            <Typography className={classes.category_header}>Grocery Store</Typography>
                            <Typography className={classes.category_discription}>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui <br />officia deserunt.</Typography>
                        </Grid>
                    </Zoom>
                }
            </Grid>
            <Grid item container className={classes.header_grid}>
                <Typography className={classes.header_txt}>{props.header}</Typography>
            </Grid>
            <Grid item container className={classes.discription_grid}>
                <Typography className={classes.discription_txt}>{props.description}</Typography>
            </Grid>
        </Grid>
    );
}

export default BrandedCategory;
