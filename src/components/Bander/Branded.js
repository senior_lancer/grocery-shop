import {Button, Grid, Typography, Box, useMediaQuery, Collapse, CardMedia } from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import BrandedImage from '../../static/images/branded.jpg';
import Products from './util/Product';
import BrandedCategory from './BrandedCategory';

import {db_branded} from '../../database/db_broduct';


const useStyles = makeStyles(theme => ({
    header_img: {
        height: '300px'
    },
    header_txt: {
        fontSize: '32px',
        [theme.breakpoints.down('sm')]: {
            fontSize: '24px',
        },
        background: '#FFFFFF',
        color: '#FA1818',
        padding: '0 18px',
        marginTop: '112px'
    },
    product_list: {
        padding: '60px 0'
    },
    product_header: {
        color: '#212121',
        fontSize: '32px'
    },
    product_area: {
        margin: '48px 0'
    },
    sub_header: {
        color:'#212121',
        fontSize: '24px',
        textTransform: 'uppercase',
        marginBottom: '24px',
        padding: '0 15px'
    },
    category_grid: {
        padding: '80px 0'
    }
}));

const category_list = [
    {
        thumbnail: '',
        header: 'Utensils',
        description: '1. sunt in culpa qui officia<br /> 2. commodo consequat<br /> 3. sed do eiusmod tempor incididunt'
    },
    {
        thumbnail: '',
        header: 'Hair Care',
        description: '1. enim ipsam voluptatem officia<br /> 2. tempora incidunt ut labore et<br /> 3. vel eum iure reprehenderit'
    },
    {
        thumbnail: '',
        header: 'Cookies',
        description: '1. dolorem eum fugiat voluptas\n 2. ut aliquid ex ea commodi<br /> 3. magnam aliquam quaerat'
    },
]

const Branded = props => {
    const classes = useStyles();

    return(
        <Grid container direction='column'>
            <CardMedia image={BrandedImage} className={classes.header_img}>
                <Grid item container justify='center'>
                    <Typography className={classes.header_txt}>Best Deals For New Products</Typography>
                </Grid>
            </CardMedia>
            <Grid item container className={classes.category_grid} direction='row' justify='center'>
                {
                    category_list.map(item => 
                        <BrandedCategory 
                            key={item.header}
                            thumbnail={item.thumbnail}
                            header={item.header}
                            description={item.description}
                        />
                    )}
            </Grid>
            <Grid item container className={classes.product_list} direction='column'>
                <Grid item container justify="center">
                    <Typography className={classes.product_header}>Households Products</Typography>
                </Grid>
                {db_branded.map(item => 
                    <Grid key={item.kind} item container className={classes.product_area} justify="center">
                        <Grid item container className={classes.cleaning_header} justify='flex-start'>
                            <Typography className={classes.sub_header}>{item.kind}</Typography>
                        </Grid>
                        <Grid item container justify="flex-start">
                            {
                                item.product.map(product => 
                                    <Products 
                                        key={product.thumbnail + product.name}
                                        thumbnail={product.thumbnail}
                                        name={product.name}
                                        current_price={product.current_price}
                                        before_price={product.before_price}
                                    />
                                )}
                        </Grid>
                    </Grid>
                )}
            </Grid>
        </Grid>
    );
}

export default Branded;