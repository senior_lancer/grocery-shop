import {Grid, Card, Typography, CardContent} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import 'react-awesome-slider/dist/styles.css';


import Caroual1 from './../../static/images/Carousel_1.jpg';
import Caroual2 from './../../static/images/Carousel_2.jpg';
import Caroual3 from './../../static/images/Carousel_3.jpg';

const useStyles = makeStyles(theme => ({
    context: {
        height: '1200px'
    }
}));

const AutoplaySlider = withAutoplay(AwesomeSlider);

const HomeProduct = props => {
    const classes = useStyles();

    return(
        <AutoplaySlider
            organicArrows={false}
            play={true}
            cancelOnInteraction={false} // should stop playing on user interaction
            interval={3000}
        >
            <div data-src={Caroual1} />
            <div data-src={Caroual2} />
            <div data-src={Caroual3} />
        </AutoplaySlider>
    );
}

export default HomeProduct;