import {Button, Grid, Typography, Box, useMediaQuery, Collapse } from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import HomeIcon from '@material-ui/icons/Home';
import MenuIcon from '@material-ui/icons/Menu';

import Siderbar from '../Siderbar';
import Product from './Product';

const useStyles = makeStyles(theme => ({
    content: {
        marginTop: '16px'        
    },
    brand_grid: {
        padding: '15px',
        background: '#84C639'
    },
    home_icon: {
        color: '#FA1818'
    },
    home_txt: {
        marginLeft: '12px',
        color: '#FFFFFF',
        '&:hover': {
            color: '#FA1818'
        },
    },
    line_txt: {
        marginLeft: '16px',
        color: '#000000'
    },
    subbrand_txt: {
        marginLeft: '16px',
        color: '#000000',
        '&:hover': {
            color: '#FFFFFF'
        },
    },
    menu_icon_grid: {
        margin: '20px',
        [theme.breakpoints.up('md')]:{
            height: '0px',
            margin: '0px'
        }
    },
    menu_icon: {
        background: '#84C639',
        color: '#FFFFFF',
        [theme.breakpoints.up('md')]:{
            height: '0px',
        }
    },
    bander_product: {
        // height: '1000px',
        // background: '#84C639'
    }
}));

const Bander = props => {
    const classes = useStyles();

    const [getSubbrand, setSubbrand] = useState("");

    const [getSiderBarStatus, setSiderBarStatus] = useState(false);

    const handleMenuClick = () => {
        setSiderBarStatus(!getSiderBarStatus);
    }

    const matches = useMediaQuery(theme => theme.breakpoints.down('md'));

    return(
        <Grid container className={classes.content} direction="column">
            <Grid item  container className={classes.brand_grid} direction="row" alignItems="center">
                <Grid item xs={false} sm={2}/>
                <HomeIcon className={classes.home_icon} fontSize='small'/>
                <Typography className={classes.home_txt}>Home</Typography>
                <Typography className={classes.line_txt}>|</Typography>
                <Typography className={classes.subbrand_txt}>{getSubbrand}</Typography>
            </Grid>
            <Grid item container direction="row" alignItems="flex-start">
                <Grid component={Box} item className={classes.menu_icon_grid} container xs={12} sm={12} justify="center">
                    <MenuIcon className={classes.menu_icon} fontSize="large" onClick={handleMenuClick} />
                </Grid>
                <Grid component={Box} item xs={12} sm={12} md={2}>
                    {matches ?
                        <Collapse in={getSiderBarStatus}>
                            <Siderbar setbrand={setSubbrand} />
                        </Collapse>  : <Siderbar setbrand={setSubbrand}/>
                    }
                </Grid>
                <Grid item className={classes.bander_product} xs={12} sm={12} md={10}>
                    <Product product={getSubbrand} />
                </Grid>
            </Grid>
        </Grid>
    );
}

export default Bander;
