import React from 'react';
import {
  fade,
  ThemeProvider,
  withStyles,
  makeStyles,
  createMuiTheme,
} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import { green } from '@material-ui/core/colors';

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: '#84C639',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#84C639',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        // borderColor: '',
      },
      '&:hover fieldset': {
        borderColor: '#84C639',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#84C639',
      },
    },
  },
})(TextField);

export default CssTextField;