import {Button, Grid, Typography, Box, useMediaQuery, Collapse } from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import SignIn from './SignIn';
import SignUp from './SignUp';

import CreateIcon from '@material-ui/icons/Create';

const useStyles = makeStyles(theme => ({
    sign_grid: {
        backgroud: '#FA1818',
        margin: '3em auto 3em'
    },
    sign_line: {
        content: '',
        background: '#FA1818',
        height: '2px',
        width: '8%',
        position: 'absolute',
        bottom: '0%',
        left: '46%',
    },
    sign_box: {
        position: 'relative',
        background: '#ffffff',
        maxWidth: '500px',
        width: '100%',
        borderTop: '5px solid #FA1818',
        boxShadow: '0 0 3px rgba(0, 0, 0, 0.25)',
        margin: '1em auto 0',
    },
    sign_content:{
        padding: '40px'
    },
    padding_10: {
        padding:'0 20px'
    },
    pen_grid: {
        padding: '3px',
        position: 'absolute',
        top: '-0',
        right: '-0',
        background: '#FA1818',
        width: '30px',
        height: '30px',
        margin: '-5px 0 0',
        color: '#ffffff',
        fontSize: '12px',
        lineHeight: '30px',
    },
    forget_grid: {
        background: '#343434',
        maxWidth: '500px',
        padding: '15px 40px',
        color: '#FFFFFF',
    },
    forget_txt: {
        fontSize: '20px'
    },
    collapse_width: {
        width: '100%'
    }
}));

const Sign = props => {
    const classes = useStyles();
    
    const [getSignIn, setSignIn] = useState(true);

    const handleSignChange = () => {
        setSignIn(!getSignIn);
    }

    return(
        <Grid container direction="column">
            <Grid item container justify="center">
                <Grid item container className={classes.sign_grid} direction="column" justify="center">
                    <Typography component="div">
                        <Box textAlign="center" fontSize="h4.fontSize">Sign In & Sign Up</Box>
                    </Typography>
                    <Grid item container className={classes.padding_10}>
                        <Grid item container className={classes.sign_box}>
                            <Grid item container className={classes.sign_content}>
                                    <Collapse className={classes.collapse_width} in={getSignIn}><SignIn /></Collapse>
                                    <Collapse className={classes.collapse_width} in={!getSignIn}><SignUp /></Collapse>
                                <Grid container className={classes.pen_grid} justify='center' onClick={handleSignChange}>
                                    <CreateIcon />
                                </Grid>
                            </Grid>
                            <Grid item container className={classes.forget_grid} justify='center'>
                                <Typography className={classes.forget_txt}>Forgot your password?</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default Sign;