import {Button, Grid, Typography, Box, useMediaQuery, TextField } from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import CSSTextField from './CssTextField ';

const useStyles = makeStyles(theme=>({
    header_txt: {
        margin: '0 0 1.5em',
        color: '#212121',
        fontSize: '1.2em',
        fontWeight: '400',
        lineHeight: 1
    },
    area: {
        marginBottom: '10px'
    },
    error_txt: {
        height: '24px',
        color: '#FA1818'
    },
    button: {
        color:'#FFFFFF',
        background: '#84C639',
        '&:hover': {
            background:  '#FA1818'
        }
    }
}));

const SignIn = props => {
    const classes = useStyles();

    const [getUserName, setUserName] = useState("");
    const [getPassword, setPassword] = useState("");

    const [getAlertName, setAlertName] = useState("");
    const [getAlertPassword, setAlertPassword] = useState("");

    const handleNameChange = (event) => {
        setUserName(event.target.value);
        setAlertName("");
    }

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
        setAlertPassword("");
    }

    const handleLoginClick = () => {
        getUserName === "" ? setAlertName('Please insert username') : setAlertName("");
        getPassword === "" ? setAlertPassword('Please insert password') : setAlertPassword("");
    }

    return(
        <Grid container direction='column'>
            <Typography className={classes.header_txt}>Login In your account</Typography>
            <Grid item container className={classes.area} direction='column'>
                <CSSTextField id="outlined-basic" label="Username" variant="outlined" onChange={handleNameChange}/>
                <Typography className={classes.error_txt}>{getAlertName}</Typography>
            </Grid>
            <Grid item container className={classes.area} direction='column'>
                <CSSTextField id="outlined-basic" label="Password" variant="outlined" type="password" onChange={handlePasswordChange}/>
                <Typography className={classes.error_txt}>{getAlertPassword}</Typography>
            </Grid>
            <Button className={classes.button} variant="contained" onClick={handleLoginClick} >Login</Button>
        </Grid>
    )
}

export default SignIn;