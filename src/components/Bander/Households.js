import {Button, Grid, Typography, Box, useMediaQuery, Collapse, CardMedia } from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import HeaderImage from '../../static/images/households.jpg';
import Products from './util/Product';

import {db_households} from '../../database/db_broduct';

const useStyles = makeStyles(theme => ({
    header_img: {
        height: '300px'
    },
    header_txt: {
        fontSize: '32px',
        [theme.breakpoints.down('sm')]: {
            fontSize: '24px',
        },
        background: '#FFFFFF',
        color: '#FA1818',
        padding: '0 18px',
        marginTop: '112px'
    },
    product_list: {
        padding: '60px 0'
    },
    product_header: {
        color: '#212121',
        fontSize: '32px'
    },
    product_area: {
        margin: '48px 0'
    },
    sub_header: {
        color:'#212121',
        fontSize: '24px',
        textTransform: 'uppercase',
        marginBottom: '24px',
        padding: '0 15px'
    }
}));

const Households = props => {
    const classes = useStyles();

    return(
        <Grid container direction='column'>
            <CardMedia image={HeaderImage} className={classes.header_img}>
                <Grid item container justify='center'>
                    <Typography className={classes.header_txt}>Best Deals For New Products</Typography>
                </Grid>
            </CardMedia>
            <Grid item container className={classes.product_list} direction='column'>
                <Grid item container justify="center">
                    <Typography className={classes.product_header}>Households Products</Typography>
                </Grid>
                {db_households.map(item => 
                    <Grid key={item.kind} item container className={classes.product_area} justify="center">
                        <Grid item container className={classes.cleaning_header} justify='flex-start'>
                            <Typography className={classes.sub_header}>{item.kind}</Typography>
                        </Grid>
                        <Grid item container justify="flex-start">
                            {
                                item.product.map(product => 
                                    <Products 
                                        key={product.thumbnail + product.name}
                                        thumbnail={product.thumbnail}
                                        name={product.name}
                                        current_price={product.current_price}
                                        before_price={product.before_price}
                                    />
                                )}
                        </Grid>
                    </Grid>
                )}
            </Grid>
        </Grid>
    );
}

export default Households;