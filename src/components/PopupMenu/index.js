import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import Grid from '@material-ui/core/Grid';
import { BrowserRouter } from 'react-router-dom';
import {connect} from 'react-redux';

import {setObjectSign, actionBanner} from '../../redux/actions';
import {BANNER_ACTION_SIGN} from '../../util/action_type'
import { Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';


const mapStateToObjectSign = state => {
  return {object_sign: state.object_sign}
}

const mapDispatchSignPopup = dispatch => {
  return {
    setObjectSign: obj_sign => dispatch(setObjectSign(obj_sign)),
    actionBanner: event => dispatch(actionBanner(event))
  }
}

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})(props => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));
 
const StyledMenuItem = withStyles(theme => ({
  root: {
    '&:hover': {
      background: '#84C639',
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

const useStyle = makeStyles(theme =>({
  link: {
    color: '#000000',
    
    '&:hover': {
      color: '#FFFFFF',
    }
  }
}));

const PopupMenu = props => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const classes = useStyle();

  const handleClick = event => {
    // setAnchorEl(event.currentTarget);
  };

  const handleClose = (event) => {
    props.setObjectSign(null);
  };

  const handleItemClick = (event) => {
    props.setObjectSign(null);
    switch(event.nativeEvent.target.outerText) {
      case 'Login':
        
        break;

      case 'Sign Up':

        break;

      default:
        break;
    }
  }

  return (
      <StyledMenu
        id="customized-menu"
        anchorEl={props.object_sign}
        keepMounted
        open={Boolean(props.object_sign)}
        onClose={handleClose}
      >
        {
          props.items.map(item => 
            <StyledMenuItem key={item.link + item.title} onClick={handleItemClick}>
              {/* <Link to={item.link}><ListItemText primary={item.title} /></Link> */}
              <Typography><Link className={classes.link} href={item.link} underline="none">{item.title}</Link></Typography>
            </StyledMenuItem>    
          )
        }
      </StyledMenu>
  );
}

export default connect(mapStateToObjectSign, mapDispatchSignPopup)(PopupMenu);