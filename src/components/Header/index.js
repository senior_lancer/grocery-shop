import {Button, Grid, Typography, AppBar} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import Search from './Search';
import Chart from './Chart';
import Sign from './Sign';

import SignPopupMenu from '../PopupMenu';

const useStyles = makeStyles(theme => ({
    appbar: {
        background: '#212121',
    },
    content: {
        background: '#212121',
        position: 'fixed',
        top: '0',
        padding: '4px',
        left: '0',
        zIndex: '100',
    },
    contact_btn:{
        marginBotton: '5px',
        background: '#FA1818',
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: '#FA1818',
        },
    }
}));

const sign_list = [
    { title:'Login', link: '/sign'}, 
    { title:'Sign Up', link: '/sign'}
    ]

const Header = props => {
    const {history} = props;
    
    const classes = useStyles();

    const [getHistory, setHistory] = useState(history);

    return(
        <Grid container className={classes.content} elevation={0}>
            <Grid item xs={12} sm={4}>
                <Search />
            </Grid>
            <Grid container item xs={12} sm={8} direction="row" alignItems="center" justify="flex-end">
                <Grid item sm={7} md={7}>
                    <Grid container justify="center">
                        <Chart />
                    </Grid>
                </Grid>
                <Grid item sm={2} md={2}>
                    <Grid container justify="center">
                        <Sign />
                        <SignPopupMenu items={sign_list} />
                    </Grid>
                </Grid>
                <Grid item sm={3} md={3}>
                    <Grid container justify="flex-end">
                        <Button className={classes.contact_btn}>Contact Us</Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Header;