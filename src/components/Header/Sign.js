import {Button, Grid, InputBase} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';
import {connect} from 'react-redux';

import PersonIcon from '@material-ui/icons/Person';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

import {setObjectSign} from '../../redux/actions';


const mapDispatchSignPopup = dispatch => {
    return {
        setObjectSign: obj_sign => dispatch(setObjectSign(obj_sign))
    }
}

const useStyles = makeStyles(theme => ({
    content: {
    },
    persion_icon: {
        color: '#FFFFFF',
        marginTop: '10px'
    },
    arrow_icon:{
        color: '#FFFFFF',
        marginTop: '10px',
        marginLeft: '-10px'
    }
}));

const Sign = props => {
    const classes = useStyles();

    const handleClick = (event) => {
        props.setObjectSign(event.currentTarget);
    }

    const handleLeave = () => {
        props.setObjectSign(null);
    }

    return(
        <Grid className={classes.content}  onClick={handleClick}>
            <PersonIcon className={classes.persion_icon} />
            <ArrowDropDownIcon className={classes.arrow_icon} />
        </Grid>
    )
}

export default connect(null, mapDispatchSignPopup)(Sign);