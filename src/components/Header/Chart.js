import {Button, Grid, Typography, InputBase} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import CartIcon from '@material-ui/icons/ShoppingCart';

const useStyles = makeStyles(theme => ({
    content: {
        float: 'left',
    },
    button: {
        color: '#ffffff',
        border: '1px solid #84c639',
    },
}));

const Chart = props => {
    const classes = useStyles();
    return(
        <Grid className={classes.content}>
            <Button
                className={classes.button}
                variant="outlined"
                size="large"
                endIcon={<CartIcon />}
            >
                View your cart
            </Button>
        </Grid>
    )   
}

export default Chart;