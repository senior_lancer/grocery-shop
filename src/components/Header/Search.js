import {Button, Grid, InputBase} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';
import SearchIcon from '@material-ui/icons/Search';


const useStyles = makeStyles(theme => ({
    content: {
        margin: ".1em 0 .1em"
    },
    search_area: {
        width: '100%',
        padding: "5px",
        color: "#212121",
        fontSize: "15px",
        background: "#fff",
    },
    search_btn: {
        marginLeft: '2px',
        minWidth: '0px',
        width: '100%',
        background: '#FA1818',
        '&:hover': {
            backgroundColor: '#FA1818'
        },
    },
    search_icon: {
        color: '#FFFFFF',
    }
}));

const Search = props => {
    const classes = useStyles();

    return(
        <Grid container className={classes.content} direction="row" justify="flex-end" alignItems="center">
            <Grid item xs={10} sm={8} >
                <InputBase 
                    className={classes.search_area}
                    placeholder="Search a product..."
                />
            </Grid>
            <Grid item xs={2} sm={2} >
                <Button 
                    className={classes.search_btn}
                    size="large"
                >
                    <SearchIcon className={classes.search_icon} />
                </Button >
            </Grid>
        </Grid>

    );
}

export default Search;