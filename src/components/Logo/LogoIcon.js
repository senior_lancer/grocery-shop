import {Button, Grid, Typography, CardMedia} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import LogoImage from '../../static/images/img-sp.png';

const useStyles = makeStyles(theme => ({
    grocery_txt: {
        fontSize: '20px',
        display: 'block',
        color: '#FA1818'
    },
    store_txt: {
        fontSize: '48px',
        color: '#212121',
        textDecoration: 'none',
        textTransform: 'uppercase',
        display: 'block',
        lineHeight: '1',
    },
    card_img: {
        height: '96px',
        marginTop: '-65px',
        background: 'no-repeat 45px 6px'
    }

}));

const LogoIcon = props => {
    const classes = useStyles();
    return(
        <Grid item>
            <Typography className={classes.grocery_txt}>GROCERY</Typography>
            <Typography className={classes.store_txt}>STOPRE</Typography>
            <CardMedia className={classes.card_img} image={LogoImage} />
        </Grid>
    );
}

export default LogoIcon;