import {Button, Grid, Typography, CardMedia} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

const useStyles = makeStyles(theme => ({
    context: {
        [theme.breakpoints.down('sm')]:{
            justifyContent: 'center'
        }
    },
    text: {
        fontSize: '18px',
        color: '#000000',
        textTransform: 'capitalize',
        marginLeft: '10px',
        [theme.breakpoints.down('xs')]: {
            marginLeft: '0px'
        }
    }
}));

const SpecialItems = props =>{
    const classes = useStyles();

    return(
        <Grid container className={classes.context} direction="row" alignItems={"center"}>
            <Button className={classes.text}>Event</Button>
            <Typography className={classes.text}>/</Typography>
            <Button className={classes.text}>About Us</Button>
            <Typography className={classes.text}>/</Typography>
            <Button className={classes.text}>Best Deals</Button>
            <Typography className={classes.text}>/</Typography>
            <Button className={classes.text}>Services</Button>
        </Grid>
    );
}

export default SpecialItems;