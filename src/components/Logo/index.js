import {Button, Grid, Typography, Link} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';
import { connect } from "react-redux";

import { actionBanner } from "../../redux/actions";

import LogoIcon from './LogoIcon';
import SpecialItems from './SpecialItems';

const useStyles = makeStyles(theme => ({
    context: {
        marginTop: "60px",
        [theme.breakpoints.down('xs')]: {
            marginTop: "120px",
        },
    },
    logo_grid: {
        justifyContent: 'flex-end',
        [theme.breakpoints.down('xs')]: {
            justifyContent:'center',
        }
    },
    special_item: {
        paddingLeft: '60px',
        [theme.breakpoints.down('sm')]:{
            paddingLeft: '0px'
        }
    }
}));

const Logo = props => {
    const classes = useStyles();

    return(
        <Grid container className={classes.context} justify="center" direction="row">
            <Grid className={classes.logo_grid} item container xs={12} sm={4}>
                <Grid>
                    <Link href="/" underline="none"><LogoIcon /></Link>
                </Grid>
            </Grid>
            <Grid className={classes.special_item} item container xs={12} sm={8}>
                <SpecialItems />
            </Grid>
        </Grid>
    );
}

export default Logo;