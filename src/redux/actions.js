import { BANNER_ACTION, SHOW_SIGN_POPUP } from '../util/action_type';

export function actionBanner(event) {
    return {type: BANNER_ACTION, event};
}

export function setObjectSign(obj_sign) {
    return {type: SHOW_SIGN_POPUP, obj_sign};
}

