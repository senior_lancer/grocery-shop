import {BANNER_ACTION, SHOW_SIGN_POPUP} from '../util/action_type';

import {BANNER_ACTION_HOME} from '../util/action_type';

const initialState = {
    banner_event: String ,
    object_sign: HTMLDivElement = null,
};

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case BANNER_ACTION:
            return Object.assign({}, state, {
                articles: state.banner_event = action.event
            });
            break;

        case SHOW_SIGN_POPUP:
            return Object.assign({}, state, {
                aa: state.object_sign = action.obj_sign
            });
            
        default:
            break;
    }
    return state;
};

export default rootReducer;