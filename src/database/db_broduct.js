export const db_households = [
    {
        kind: 'cleaning',
        product: [
            {
                thumbnail: '',
                name: 'Dishwash Gel, Lemon (1.5 Ltr)',
                current_price: '8.00',
                before_price: '10.00',
            },
            {
                thumbnail: '',
                name: 'Dish Wash Bar (500 Gm)',
                current_price: '2.00',
                before_price: '4.00',
            },
            {
                thumbnail: '',
                name: 'Air Freshener (50 Gm)',
                current_price: '3.00',
                before_price: '5.00',
            },
            {
                thumbnail: '',
                name: 'Toilet Cleaner Expert (1 Ltr)',
                current_price: '6.00',
                before_price: '7.00',
            }
        ]
    },
    {
        kind: 'utensils',
        product: [
            {
                thumbnail: '',
                name: 'Princeware Packaging Container Pack (6 No\'s)',
                current_price: '8.00',
                before_price: '10.00',
            },
            {
                thumbnail: '',
                name: 'Signoraware Container Center Press (900 Ml)',
                current_price: '5.00',
                before_price: '8.00',
            },
            {
                thumbnail: '',
                name: 'Ship Stainless Steel Sauce Pan Single (1 Pc)',
                current_price: '6.00',
                before_price: '8.00',
            },
            {
                thumbnail: '',
                name: 'Omega Stainless Steel Puri Dabba (1 Pc)',
                current_price: '6.00',
                before_price: '8.00',
            }
        ]
    },
    {
        kind: 'pet food',
        product: [
            {
                thumbnail: '',
                name: 'Food For Adult Dogs (80 Gms)',
                current_price: '3.00',
                before_price: '4.00',
            },
            {
                thumbnail: '',
                name: 'Young Adult Dogs (1.2 Kg)',
                current_price: '6.00',
                before_price: '10.00',
            },
            {
                thumbnail: '',
                name: 'Cat Food Ocean Fish (1.4 Kg)',
                current_price: '6.00',
                before_price: '7.00',
            },
            {
                thumbnail: '',
                name: 'Chicken In Jelly Can (400 Gm)',
                current_price: '7.00',
                before_price: '9.00',
            }
        ]
    },
]

export const db_branded = [
    {
        kind: 'FOOD',
        product: [
            {
                thumbnail: '',
                name: 'Knorr Instant Soup (100 Gm)',
                current_price: '3.00',
                before_price: '5.00',
            },
            {
                thumbnail: '',
                name: 'Chings Noodles (75 Gm)',
                current_price: '5.00',
                before_price: '8.00',
            },
            {
                thumbnail: '',
                name: 'Lahsun Sev (150 Gm)',
                current_price: '3.00',
                before_price: '5.00',
            },
            {
                thumbnail: '',
                name: 'Premium Bake Rusk (300 Gm)',
                current_price: '5.00',
                before_price: '7.00',
            }
        ]
    },
    {
        kind: 'VEGETABLES & FRUITS',
        product: [
            {
                thumbnail: '',
                name: 'Fresh Spinach (Palak)',
                current_price: '2.00',
                before_price: '3.00',
            },
            {
                thumbnail: '',
                name: 'Fresh Mango Dasheri (1 Kg)',
                current_price: '5.00',
                before_price: '8.00',
            },
            {
                thumbnail: '',
                name: 'Fresh Apple Red (1 Kg)',
                current_price: '6.00',
                before_price: '8.00',
            },
            {
                thumbnail: '',
                name: 'Fresh Broccoli (500 Gm)',
                current_price: '4.00',
                before_price: '6.00',
            }
        ]
    },
    {
        kind: 'BEVERAGES',
        product: [
            {
                thumbnail: '',
                name: 'Mixed Fruit Juice (1 Ltr)',
                current_price: '3.00',
                before_price: '4.00',
            },
            {
                thumbnail: '',
                name: 'Prune Juice - Sunsweet (1 Ltr)',
                current_price: '4.00',
                before_price: '5.00',
            },
            {
                thumbnail: '',
                name: 'Coco Cola Zero Can (330 Ml)',
                current_price: '3.00',
                before_price: '5.00',
            },
            {
                thumbnail: '',
                name: 'Sprite Bottle (2 Ltr)',
                current_price: '3.00',
                before_price: '4.00',
            }
        ]
    },
]