import {Button, Grid, Typography} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import React, { useState } from 'react';

import Header from '../components/Header';
import Logo from '../components/Logo';
import Bander from '../components/Bander';
import Footer from '../components/Footer';

const useStyles = makeStyles(theme => ({
    
}));

const Home = props => {
    const {history} = props;
    
    const classes = useStyles();

    const [getHistory, setHistory] = useState(history);

    return(
        <Grid >
           <Header />
           <Logo />
           <Bander />
           <Footer />
        </Grid>
    );
};

export default Home;